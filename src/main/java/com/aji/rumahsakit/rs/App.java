package com.aji.rumahsakit.rs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication 
@EnableAutoConfiguration
@ComponentScan(basePackages= "com.aji.rumahsakit.rs")
@EnableJpaRepositories(basePackages = "com.aji.rumahsakit.rs.dao")
@EnableTransactionManagement
@EntityScan(basePackages= {"com.aji.rumahsakit.rs.model"})
public class App
{	
    public static void main( String[] args )
    {
    	SpringApplication.run(App.class, args);
    }
}
